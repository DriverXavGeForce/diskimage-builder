The goal of this element is twofold:
* Ensure that cloud-init is really enabled
* Fix rendering of network by cloud-init by integrating the latest changes
  of cloud-init for rendering VLAN.
* Recompile with the right python (ubuntu bionic)


